<?php

	use PHPUnit\Framework\TestCase;

    include_once __DIR__.'/../../src/Coordinates.php';

	class CoordinatesTest extends testCase {

		public function testXY ( ) {
	
	        $x = 7.0;
	        $y = 42.0;
	
			$coordinates = new Coordinates($x, $y);
	        $this->assertEquals($coordinates->getX(),$x);
            $this->assertEquals($coordinates->getY(),$y);
	    }

	    public function testCast ( ) {

	        $x = 7;
	        $y = 42;

            $coordinates = new Coordinates($x, $y);
            $this->assertTrue(is_float($coordinates->getX()));
            $this->assertTrue(is_float($coordinates->getY()));
	    }
	}

